FROM python:latest

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN pip install flask
RUN python ./src/main.py -p 8000
RUN pip install coverage
RUN coverage run ./src/main.py
RUN coverage report ./src/main.py
RUN pip install -U pytest
RUN pytest --version
RUN pytest ./tests/**
RUN coverage run -m pytest ./tests/**
RUN coverage report -m pytest ./tests/**
RUN coverage html

EXPOSE 8000

CMD ["python", "/usr/src/app/main.py", "-p 8000"]
